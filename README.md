# Web-Skeleton

Skeleton for php-application. Uses docker as base.  
Consists of nginx, php, MySQL and phpmyadmin.

Ready for development

## Detach

```bash
rm -rf .git && git init
```

## Install

```bash
docker-compose -f docker-compose.development.yml up -d
composer install
```

## ToDo

| Where                           | What                                                        |
|---------------------------------|-------------------------------------------------------------|
| `docker-compose.development.yml`| Replace `skeleton` with your project name                   |
| `composer.json`                 | Set `name`, `description` and autoloading namespace         |
| `data/sql`                      | Adjust to your needs                                        |
|                                 |                                                             |
|                                 |                                                             |
|                                 |                                                             |
|                                 |                                                             |
|                                 |                                                             |
|                                 |                                                             |
