CREATE TABLE `gemstone_tags`
(
    `gemstone_id` char(8) NOT NULL,
    `tag_id`      char(8) NOT NULL,
    PRIMARY KEY (`gemstone_id`, `tag_id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_general_ci;
