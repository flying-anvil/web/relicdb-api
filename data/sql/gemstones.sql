CREATE TABLE `gemstones`
(
    `id`          char(8)       NOT NULL,
    `name`        varchar(64)   NOT NULL,
    `color`       varchar(24)   NOT NULL,
    `description` varchar(1024) NOT NULL,
    `create_date` datetime      NOT NULL,
    PRIMARY KEY (`id`),
    UNIQUE KEY (`name`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_general_ci;
