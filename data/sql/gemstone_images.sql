CREATE TABLE `gemstone_images`
(
    `gemstone_id` char(8) NOT NULL,
    `image_id`    char(8) NOT NULL,
    PRIMARY KEY (`gemstone_id`, `image_id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_general_ci;
