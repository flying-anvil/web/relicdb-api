CREATE TABLE `gemstone_references`
(
    `gemstone_id`  char(8) NOT NULL,
    `reference_id` char(8) NOT NULL,
    PRIMARY KEY (`gemstone_id`, `reference_id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_general_ci;
