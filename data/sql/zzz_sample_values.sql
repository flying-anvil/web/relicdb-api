INSERT IGNORE INTO `tags` (id, name, create_date) VALUES
('bit-0001', 'natural', '2021-08-12 17:25:00'),
('bit-0002', 'real', '2021-08-12 17:25:00'),
('bit-0003', 'fictional', '2021-08-12 17:25:00')
;

INSERT IGNORE INTO `gemstones` (id, name, color, description, create_date) VALUES
('big-0003', 'Mithril', 'silver', '', '2021-08-13 16:01:27')
;

# INSERT INTO `images` (id, source, title) VALUES

# INSERT INTO `gemstone_images` (gemstone_id, image_id) VALUES
# ('bit-0003', )

INSERT IGNORE INTO `references` (id, source, title) VALUES
('bir-0003', 'https://de.wikipedia.org/wiki/Mithril', 'Wikipedia')
;

INSERT IGNORE INTO `gemstone_references` (gemstone_id, reference_id) VALUES
('big-0003', 'bir-0003')
;

INSERT IGNORE INTO `gemstone_tags` (gemstone_id, tag_id) VALUES
('big-0003', 'bit-0003')
;
