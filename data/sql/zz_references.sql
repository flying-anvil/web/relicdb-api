ALTER TABLE gemstone_images
    ADD FOREIGN KEY (`gemstone_id`) REFERENCES gemstones(id) ON DELETE CASCADE,
    ADD FOREIGN KEY (`image_id`) REFERENCES images(id) ON DELETE CASCADE;

ALTER TABLE gemstone_references
    ADD FOREIGN KEY (`gemstone_id`) REFERENCES gemstones(id) ON DELETE CASCADE,
    ADD FOREIGN KEY (`reference_id`) REFERENCES `references`(id) ON DELETE CASCADE;

ALTER TABLE gemstone_tags
    ADD FOREIGN KEY (`gemstone_id`) REFERENCES gemstones(id) ON DELETE CASCADE,
    ADD FOREIGN KEY (`tag_id`) REFERENCES tags(id) ON DELETE CASCADE;
