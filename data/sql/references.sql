CREATE TABLE `references`
(
    `id`     char(8)      NOT NULL,
    `source` varchar(255) NOT NULL,
    `title`  varchar(100) NOT NULL,
    PRIMARY KEY (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_general_ci;
