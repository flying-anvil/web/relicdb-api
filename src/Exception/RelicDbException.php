<?php

declare(strict_types=1);

namespace FlyingAnvil\RelicDbApi\Exception;

use FlyingAnvil\Libfa\Exception\FlyingAnvilException;

class RelicDbException extends FlyingAnvilException
{
}
