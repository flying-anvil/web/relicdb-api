<?php

declare(strict_types=1);

namespace FlyingAnvil\RelicDbApi\Repository;

use FlyingAnvil\Libfa\DataObject\Id\SmallId;
use FlyingAnvil\Libfa\DataObject\Time\UtcDate;
use FlyingAnvil\RelicDbApi\DataObjects\Collection\Tags;
use FlyingAnvil\RelicDbApi\DataObjects\Tag;
use PDO;

class TagRepository
{
    public function __construct(
        private PDO $pdo,
    ) {}

    public function loadAllTags(): Tags
    {
        $sql = 'SELECT * FROM tags';
        $statement = $this->pdo->query($sql);

        $tags = [];
        foreach ($statement as $row) {
            $tags[] = Tag::create(
                SmallId::createFromString($row['id']),
                $row['name'],
                UtcDate::parse($row['create_date']),
            );
        }

        return Tags::create(...$tags);
    }
}
