<?php

declare(strict_types=1);

namespace FlyingAnvil\RelicDbApi\Repository\Exception;

use FlyingAnvil\RelicDbApi\Exception\RelicDbException;

class DatabaseException extends RelicDbException
{
}
