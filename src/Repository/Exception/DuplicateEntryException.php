<?php

declare(strict_types=1);

namespace FlyingAnvil\RelicDbApi\Repository\Exception;

use Throwable;

class DuplicateEntryException extends DatabaseException
{
    public function __construct(
        private string $field,
        string $message = '',
        int $code = 0,
        Throwable $previous = null,
    ) {
        parent::__construct($message, $code, $previous);
    }

    public function getField(): string
    {
        return $this->field;
    }
}
