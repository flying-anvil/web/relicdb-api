<?php

declare(strict_types=1);

namespace FlyingAnvil\RelicDbApi\Repository;

use FlyingAnvil\Libfa\DataObject\Id\SmallId;
use FlyingAnvil\Libfa\DataObject\Time\UtcDate;
use FlyingAnvil\RelicDbApi\DataObjects\Collection\Gemstones;
use FlyingAnvil\RelicDbApi\DataObjects\Collection\Images;
use FlyingAnvil\RelicDbApi\DataObjects\Collection\References;
use FlyingAnvil\RelicDbApi\DataObjects\Collection\Tags;
use FlyingAnvil\RelicDbApi\DataObjects\Gemstone;
use FlyingAnvil\RelicDbApi\DataObjects\Image;
use FlyingAnvil\RelicDbApi\DataObjects\Reference;
use FlyingAnvil\RelicDbApi\DataObjects\Tag;
use FlyingAnvil\RelicDbApi\Repository\Exception\DuplicateEntryException;
use PDO;
use PDOException;

class GemstoneRepository
{
    private array $statementCache = [];

    public function __construct(
        private PDO $pdo,
    ) {}

    public function loadAllGemstones(): Gemstones
    {
        $sqlGemstones = 'SELECT * FROM gemstones';

        $gemstones = [];
        foreach ($this->pdo->query($sqlGemstones) as $gemstoneRow) {
            $gemstoneId = SmallId::createFromString($gemstoneRow['id']);
            $gemstones[] = Gemstone::create(
                $gemstoneId,
                $gemstoneRow['name'],
                $gemstoneRow['color'],
                $gemstoneRow['description'],
                $this->loadImagesForGemstone($gemstoneId),
                $this->loadReferencesForGemstone($gemstoneId),
                $this->loadTagsForGemstone($gemstoneId),
                UtcDate::parse($gemstoneRow['create_date']),
            );
        }

        return Gemstones::create(...$gemstones);
    }

    private function loadImagesForGemstone(SmallId $gemstoneId): Images
    {
        if (!isset($this->statementCache['images'])) {
            $sqlImages = <<< SQL
                SELECT i.id, i.source, i.title FROM images i
                LEFT OUTER JOIN gemstone_images gi on i.id = gi.image_id
                WHERE gi.gemstone_id = :gemstoneId
            SQL;

            $this->statementCache['images'] = $this->pdo->prepare($sqlImages);
        }

        $this->statementCache['images']->execute([
            'gemstoneId' => $gemstoneId->toString(),
        ]);

        $images = [];
        foreach ($this->statementCache['images'] as $imageRow) {
            $images[] = Image::create(
                SmallId::createFromString($imageRow['id']),
                $imageRow['source'],
                $imageRow['title'],
            );
        }

        return Images::create(...$images);
    }

    private function loadReferencesForGemstone(SmallId $gemstoneId): References
    {
        if (!isset($this->statementCache['reference'])) {
            $sqlReferences = <<< SQL
                SELECT r.id, r.source, r.title FROM `references` r
                LEFT JOIN gemstone_references gr on r.id = gr.reference_id
                WHERE gr.gemstone_id = :gemstoneId
            SQL;

            $this->statementCache['reference'] = $this->pdo->prepare($sqlReferences);
        }

        $this->statementCache['reference']->execute([
            'gemstoneId' => $gemstoneId->toString(),
        ]);

        $references = [];
        foreach ($this->statementCache['reference'] as $referenceRow) {
            $references[] = Reference::create(
                SmallId::createFromString($referenceRow['id']),
                $referenceRow['source'],
                $referenceRow['title'],
            );
        }

        return References::create(...$references);
    }

    private function loadTagsForGemstone(SmallId $gemstoneId): Tags
    {
        if (!isset($this->statementCache['tags'])) {
            $sqlTags = <<< SQL
                SELECT t.id, t.name, t.create_date FROM tags t
                LEFT OUTER JOIN gemstone_tags gt on t.id = gt.tag_id
                WHERE gt.gemstone_id = :gemstoneId
            SQL;

            $this->statementCache['tags'] = $this->pdo->prepare($sqlTags);
        }

        $this->statementCache['tags']->execute([
            'gemstoneId' => $gemstoneId->toString(),
        ]);

        $tags = [];
        foreach ($this->statementCache['tags'] as $tagRow) {
            $tags[] = Tag::create(
                SmallId::createFromString($tagRow['id']),
                $tagRow['name'],
                UtcDate::parse($tagRow['create_date']),
            );
        }

        return Tags::create(...$tags);
    }

    public function countAll(): int
    {
        $sql = 'SELECT COUNT(id) as count FROM gemstones';
        return (int)$this->pdo->query($sql)->fetch()['count'];
    }

    public function getLatestGemstoneDate(): UtcDate
    {
        $sql  = 'SELECT create_date FROM gemstones ORDER BY create_date LIMIT 1';
        $date = $this->pdo->query($sql)->fetch()['create_date'];

        return UtcDate::parse($date);
    }

    public function insertGemstone(Gemstone $gemstone): void
    {
        $this->pdo->beginTransaction();

        try {
            // Gemstone
            $sqlGemstone = <<< SQL
                INSERT INTO gemstones (id, name, color, description, create_date) VALUES
                (:id, :name, :color, :description, :createDate)
            SQL;

            $gemstoneId = $gemstone->getId()->toString();
            $this->pdo->prepare($sqlGemstone)->execute([
                'id'          => $gemstoneId,
                'name'        => $gemstone->getName(),
                'color'       => $gemstone->getColor(),
                'description' => $gemstone->getDescription(),
                'createDate'  => $gemstone->getCreationDate()->format(),
            ]);

            // Images
            if ($gemstone->getImages()->count() > 0) {
                $sqlImages     = 'INSERT INTO images (id, source, title) VALUES';
                $sqlImageLinks = 'INSERT INTO gemstone_images (gemstone_id, image_id) VALUES';
                $imageParameters      = [];
                $imageLinksParameters = [];
                foreach ($gemstone->getImages() as $index => $image) {
                    $imageId = $image->getId()->toString();
                    $sqlImages .= sprintf(
                        '%2$s (:id_%1$s, :source_%1$s, :title_%1$s)',
                        $index,
                        $index === 0 ? '' : ',',
                    );

                    $imageParameters["id_$index"]     = $imageId;
                    $imageParameters["source_$index"] = $image->getSource();
                    $imageParameters["title_$index"]  = $image->getTitle();

                    $sqlImageLinks .= sprintf(
                        '%2$s (:gemstone_id_%1$s, :image_id_%1$s)',
                        $index,
                        $index === 0 ? '' : ',',
                    );

                    $imageLinksParameters["gemstone_id_$index"] = $gemstoneId;
                    $imageLinksParameters["image_id_$index"]    = $imageId;
                }

                $this->pdo->prepare($sqlImages)->execute($imageParameters);
                $this->pdo->prepare($sqlImageLinks)->execute($imageLinksParameters);
            }

            // References
            if ($gemstone->getReferences()->count() > 0) {
                $sqlReferences = 'INSERT INTO `references` (id, source, title) VALUES';
                $sqlReferenceLinks = 'INSERT INTO gemstone_references (gemstone_id, reference_id) VALUES';
                $referenceParameters = [];
                $referenceLinksParameters = [];
                foreach ($gemstone->getReferences() as $index => $reference) {
                    $referenceId = $reference->getId()->toString();
                    $sqlReferences .= sprintf(
                        '%2$s (:id_%1$s, :source_%1$s, :title_%1$s)',
                        $index,
                        $index === 0 ? '' : ',',
                    );

                    $referenceParameters["id_$index"]     = $referenceId;
                    $referenceParameters["source_$index"] = $reference->getSource();
                    $referenceParameters["title_$index"]  = $reference->getTitle();

                    $sqlReferenceLinks .= sprintf(
                        '%2$s (:gemstone_id_%1$s, :reference_id_%1$s)',
                        $index,
                        $index === 0 ? '' : ',',
                    );

                    $referenceLinksParameters["gemstone_id_$index"]  = $gemstoneId;
                    $referenceLinksParameters["reference_id_$index"] = $referenceId;
                }

                $this->pdo->prepare($sqlReferences)->execute($referenceParameters);
                $this->pdo->prepare($sqlReferenceLinks)->execute($referenceLinksParameters);
            }

            // Tags
            if ($gemstone->getTags()->count() > 0) {
                $sqlTagLinks = 'INSERT INTO gemstone_tags (gemstone_id, tag_id) VALUES';
                $tagLinksParameters = [];
                foreach ($gemstone->getTags() as $index => $tag) {
                    $sqlTagLinks .= sprintf(
                        '%2$s (:gemstone_id_%1$s, :tag_id_%1$s)',
                        $index,
                        $index === 0 ? '' : ',',
                    );

                    $tagLinksParameters["gemstone_id_$index"] = $gemstoneId;
                    $tagLinksParameters["tag_id_$index"]      = $tag->getId()->toString();
                }

                $this->pdo->prepare($sqlTagLinks)->execute($tagLinksParameters);
            }

            $this->pdo->commit();
        } catch (PDOException $exception) {
            $this->pdo->rollBack();

            // Duplicate entry
            if ($exception->getCode() === '23000') {
                // Duplicate entry 'Emerald' for key 'gemstones.name'
                preg_match('/Duplicate entry \'(.+)\' for key \'([^.]+).([^.]+)\'/', $exception->getMessage(), $matches);
                $errorMessage = sprintf(
                    '%s with %s "%s" already exists',
                    ucfirst($matches[2]),
                    $matches[3],
                    ucfirst($matches[1]),
                );

                throw new DuplicateEntryException($matches[3], $errorMessage, previous: $exception);
            }

            throw $exception;
        }
    }
}
