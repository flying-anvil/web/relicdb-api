<?php

declare(strict_types=1);

namespace FlyingAnvil\RelicDbApi\Api\Tags;

use FlyingAnvil\RelicDbApi\Repository\TagRepository;
use FlyingAnvil\RelicDbApi\Slim\AbstractAction;
use Psr\Http\Message\ResponseInterface;
use Slim\Http\Response;
use Slim\Http\ServerRequest;

class GetTagsAction extends AbstractAction
{
    public function __construct(
        private TagRepository $tagsRepository,
    ) {}

    public function __invoke(ServerRequest $request, Response $response, array $routeParams = []): ResponseInterface
    {
        return $response->withJson([
            'tags' => $this->tagsRepository->loadAllTags(),
        ]);
    }
}
