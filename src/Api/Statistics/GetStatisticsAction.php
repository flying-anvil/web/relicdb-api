<?php

declare(strict_types=1);

namespace FlyingAnvil\RelicDbApi\Api\Statistics;

use FlyingAnvil\RelicDbApi\Repository\GemstoneRepository;
use FlyingAnvil\RelicDbApi\Slim\AbstractAction;
use Psr\Http\Message\ResponseInterface;
use Slim\Http\Response;
use Slim\Http\ServerRequest;

class GetStatisticsAction extends AbstractAction
{
    public function __construct(
        private GemstoneRepository $gemstoneRepository,
    ) {}

    public function __invoke(ServerRequest $request, Response $response, array $routeParams = []): ResponseInterface
    {
        return $response->withJson([
            'relics'    => [
                'count'        => 0,
                'latestUpdate' => null,
            ],
            'gemstones' => [
                'count'        => $this->gemstoneRepository ->countAll(),
                'latestUpdate' => $this->gemstoneRepository->getLatestGemstoneDate(),
            ],
        ]);
    }
}
