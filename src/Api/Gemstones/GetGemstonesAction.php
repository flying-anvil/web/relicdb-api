<?php

declare(strict_types=1);

namespace FlyingAnvil\RelicDbApi\Api\Gemstones;

use FlyingAnvil\RelicDbApi\Repository\GemstoneRepository;
use FlyingAnvil\RelicDbApi\Slim\AbstractAction;
use Psr\Http\Message\ResponseInterface;
use Slim\Http\Response;
use Slim\Http\ServerRequest;

class GetGemstonesAction extends AbstractAction
{
    public function __construct(
        private GemstoneRepository $gemstoneRepository,
    ) {}

    public function __invoke(ServerRequest $request, Response $response, array $routeParams = []): ResponseInterface
    {
        return $response->withJson([
            'gemstones' => $this->gemstoneRepository->loadAllGemstones(),
        ]);
    }
}
