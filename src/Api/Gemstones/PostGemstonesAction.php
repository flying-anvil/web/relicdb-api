<?php

declare(strict_types=1);

namespace FlyingAnvil\RelicDbApi\Api\Gemstones;

use FlyingAnvil\Libfa\DataObject\Id\SmallId;
use FlyingAnvil\Libfa\DataObject\Time\UtcDate;
use FlyingAnvil\RelicDbApi\DataObjects\Collection\Images;
use FlyingAnvil\RelicDbApi\DataObjects\Collection\References;
use FlyingAnvil\RelicDbApi\DataObjects\Collection\Tags;
use FlyingAnvil\RelicDbApi\DataObjects\Gemstone;
use FlyingAnvil\RelicDbApi\DataObjects\Image;
use FlyingAnvil\RelicDbApi\DataObjects\Reference;
use FlyingAnvil\RelicDbApi\DataObjects\Tag;
use FlyingAnvil\RelicDbApi\Repository\Exception\DatabaseException;
use FlyingAnvil\RelicDbApi\Repository\Exception\DuplicateEntryException;
use FlyingAnvil\RelicDbApi\Repository\GemstoneRepository;
use FlyingAnvil\RelicDbApi\Slim\AbstractAction;
use Psr\Http\Message\ResponseInterface;
use Slim\Http\Response;
use Slim\Http\ServerRequest;

class PostGemstonesAction extends AbstractAction
{
    public function __construct(
        private GemstoneRepository $gemstoneRepository,
    ) {}

    public function __invoke(ServerRequest $request, Response $response, array $routeParams = []): ResponseInterface
    {
//        $requestTime = $request->getServerParam('REQUEST_TIME');
        $body = $request->getParsedBody();

        if ($body === null) {
            return $this->formatError($response, 'No data found, check "Content-Type" header');
        }

        // TODO: input validation

        $gemstone = Gemstone::create(
            SmallId::generate(),
            $body['name'],
            $body['color'],
            $body['description'],
            $this->createImages($body['images']),
            $this->createReferences($body['references']),
            $this->createDummyTags($body['tags']),
            UtcDate::now(),
        );

        try {
            $this->gemstoneRepository->insertGemstone($gemstone);
        } catch (DuplicateEntryException $exception) {
            return $this->formatError($response, $exception->getMessage(), self::STATUS_CONFLICT, [
                'fields' => [
                    $exception->getField() => 'Already exists',
                ],
            ]);
        }

        return $response->withJson(['id' => SmallId::generate()]);
    }

    private function createImages(array $rawImages): Images
    {
        $images = [];
        foreach ($rawImages as $rawImage) {
            $images[] = Image::create(
                SmallId::generate(),
                $rawImage['source'],
                $rawImage['title'],
            );
        }

        return Images::create(...$images);
    }

    private function createReferences(array $rawReferences): References
    {
        $references = [];
        foreach ($rawReferences as $rawReference) {
            $references[] = Reference::create(
                SmallId::generate(),
                $rawReference['source'],
                $rawReference['title'],
            );
        }

        return References::create(...$references);
    }

    private function createDummyTags(array $rawTags): Tags
    {
        $tags = [];
        foreach ($rawTags as $rawTag) {
            $tags[] = Tag::create(
                SmallId::createFromString($rawTag),
                'dummy',
                UtcDate::now(),
            );
        }

        return Tags::create(...$tags);
    }
}
