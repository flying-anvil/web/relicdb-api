<?php

declare(strict_types=1);

namespace FlyingAnvil\RelicDbApi;

use DI\Container;
use DI\ContainerBuilder;
use FlyingAnvil\Libfa\DataObject\Application\AppEnv;
use FlyingAnvil\Libfa\Repository\EnvironmentRepository;
use FlyingAnvil\RelicDbApi\Client\Factory\PdoFactory;
use FlyingAnvil\RelicDbApi\Logger\Factory\LoggerFactory;
use FlyingAnvil\RelicDbApi\Options\Factory\LoggingOptionsFactory;
use FlyingAnvil\RelicDbApi\Options\LoggingOptions;
use Monolog\Logger;
use PDO;
use Psr\Log\LoggerInterface;

use function DI\factory;
use function DI\get;

final class ContainerFactory
{
    public static function create(): Container
    {
        $envRepo = new EnvironmentRepository();

        $containerBuilder = new ContainerBuilder();
        $containerBuilder->addDefinitions([
            AppEnv::class          => AppEnv::create($envRepo->getEnvironmentVariable('APP_ENV')),
            Logger::class          => factory(LoggerFactory::class),
            LoggerInterface::class => get(Logger::class),

            PDO::class => factory(PdoFactory::class),

            LoggingOptions::class => factory(LoggingOptionsFactory::class),

            'loggingOptions' => [
                'logLevel'     => $envRepo->getEnvironmentVariable('LOG_LEVEL', 'INFO'),
                'logDirectory' => $envRepo->getEnvironmentVariable('LOG_DIRECTORY'),
            ],
        ]);

        return $containerBuilder->build();
    }
}
