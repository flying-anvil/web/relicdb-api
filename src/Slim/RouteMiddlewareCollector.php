<?php

declare(strict_types=1);

namespace FlyingAnvil\RelicDbApi\Slim;

use Fig\Http\Message\StatusCodeInterface;
use FlyingAnvil\Libfa\DataObject\Application\AppEnv;
use FlyingAnvil\RelicDbApi\Api\Gemstones\GetGemstonesAction;
use FlyingAnvil\RelicDbApi\Api\Gemstones\PostGemstonesAction;
use FlyingAnvil\RelicDbApi\Api\Middleware\CorsMiddleware;
use FlyingAnvil\RelicDbApi\Api\Statistics\GetStatisticsAction;
use FlyingAnvil\RelicDbApi\Api\Status\StatusAction;
use FlyingAnvil\RelicDbApi\Api\Tags\GetTagsAction;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Log\LoggerInterface;
use Slim\App;
use Slim\Http\Response;
use Slim\Http\ServerRequest;
use Slim\Routing\RouteCollectorProxy;
use Throwable;

final class RouteMiddlewareCollector
{
    public function register(App $app, AppEnv $appEnv): void
    {
        $app->add(CorsMiddleware::class);

        $app->addRoutingMiddleware();

        $this->registerPreflight($app, $appEnv);
        $this->registerErrorMiddleware($app);
        $this->registerApiRoutes($app);
        $this->registerNotFoundRoutes($app);
    }

    public function registerPreflight(App $app, AppEnv $appEnv): void
    {
        $app->options('/{routes:.+}', function (ServerRequest $request, Response $response): Response {
            // Do nothing here. Just return the response.
            return $response;
        });
    }

    private function registerApiRoutes(App $app): void
    {
        $app->group('/api', function (RouteCollectorProxy $group) {
            $routes = [
                $group->get('/status', StatusAction::class)->setName('api-status'),
                $group->get('/tags', GetTagsAction::class)->setName('api-tags-list'),

                $group->get('/gemstones', GetGemstonesAction::class)->setName('api-gemstones-list'),
                $group->post('/gemstones', PostGemstonesAction::class)->setName('api-gemstones-post'),

                $group->get('/statistics', GetStatisticsAction::class)->setName('api-statistics-all'),
            ];

            $middlewares = array_reverse([]);

            foreach ($routes as $route) {
                foreach ($middlewares as $middleware) {
                    $route->add($middleware);
                }
            }
        });
    }

    public function registerNotFoundRoutes(App $app): void
    {
        $callback = function (ServerRequestInterface $request, ResponseInterface $response) {
            $content404 = '404 - Not Found';
            return $response->withStatus(404)
                ->withHeader('Content-Type', 'text/html')
                ->write($content404);
        };

        $callbackApi = function (ServerRequestInterface $request, Response $response) {
            return $response->withStatus(404)
                ->withJson([
                'message' => 'Not Found',
            ]);
        };

        $app->map(['GET', 'POST', 'PUT', 'DELETE', 'PATCH'], '/api/{routes:.+}', $callbackApi);
        $app->map(['GET', 'POST', 'PUT', 'DELETE', 'PATCH'], '/{routes:(?!api).+}', $callback);
    }

    private function registerErrorMiddleware(App $app): void
    {
        $errorMiddleware = $app->addErrorMiddleware(true, true, true);
        $errorMiddleware->setDefaultErrorHandler(function (
            ServerRequestInterface $request,
            Throwable $exception,
            bool $displayErrorDetails,
            bool $logErrors,
            bool $logErrorDetails,
            ?LoggerInterface $logger = null
        ) use ($app): Response {
            $payload = [
                'type'  => $exception::class,
                'error' => $exception->getMessage(),
                'occurred' => sprintf(
                    '%s:%d',
                    $exception->getFile(),
                    $exception->getLine(),
                ),
            ];

            $response = $app->getResponseFactory()->createResponse();

            /** @var Response $response */
            return $response
                ->withHeader('Access-Control-Allow-Origin', '*')
                ->withHeader('Access-Control-Allow-Credentials', 'true')
                ->withStatus(StatusCodeInterface::STATUS_INTERNAL_SERVER_ERROR)
                ->withJson($payload, options: JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES);
        });
    }
}
