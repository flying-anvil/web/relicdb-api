<?php

declare(strict_types=1);

namespace FlyingAnvil\RelicDbApi\DataObjects;

use FlyingAnvil\Libfa\DataObject\DataObject;
use FlyingAnvil\Libfa\DataObject\Id\SmallId;
use FlyingAnvil\Libfa\DataObject\Time\UtcDate;
use JetBrains\PhpStorm\Immutable;

#[Immutable]
final class Tag implements DataObject
{
    private function __construct(
        private SmallId $id,
        private string $name,
        private UtcDate $creationDate,
    ) {}

    public static function create(SmallId $id, string $name, UtcDate $creationDate): self
    {
        return new self($id, $name, $creationDate);
    }

    public function getId(): SmallId
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getCreationDate(): UtcDate
    {
        return $this->creationDate;
    }

    public function jsonSerialize(): array
    {
        return [
            'id'           => $this->id,
            'name'         => $this->name,
            'creationDate' => $this->creationDate,
        ];
    }
}
