<?php

declare(strict_types=1);

namespace FlyingAnvil\RelicDbApi\DataObjects;

use FlyingAnvil\Libfa\DataObject\DataObject;
use FlyingAnvil\Libfa\DataObject\Id\SmallId;
use FlyingAnvil\Libfa\DataObject\Time\UtcDate;
use FlyingAnvil\RelicDbApi\DataObjects\Collection\Images;
use FlyingAnvil\RelicDbApi\DataObjects\Collection\References;
use FlyingAnvil\RelicDbApi\DataObjects\Collection\Tags;
use JetBrains\PhpStorm\Immutable;

#[Immutable]
final class Gemstone implements DataObject
{
    private function __construct(
        private SmallId $id,
        private string $name,
        private string $color,
        private string $description,
        private Images $images,
        private References $references,
        private Tags $tags,
        private UtcDate $creationDate,
    ) {}

    public static function create(
        SmallId $id,
        string $name,
        string $color,
        string $description,
        Images $images,
        References $references,
        Tags $tags,
        UtcDate $creationDate,
    ): self {
        return new self($id, $name, $color, $description, $images, $references, $tags, $creationDate);
    }

    public function getId(): SmallId
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getColor(): string
    {
        return $this->color;
    }

    public function getDescription(): string
    {
        return $this->description;
    }

    public function getImages(): Images
    {
        return $this->images;
    }

    public function getReferences(): References
    {
        return $this->references;
    }

    public function getTags(): Tags
    {
        return $this->tags;
    }

    public function getCreationDate(): UtcDate
    {
        return $this->creationDate;
    }

    public function jsonSerialize(): array
    {
        return [
            'id'           => $this->id,
            'name'         => $this->name,
            'color'        => $this->color,
            'description'  => $this->description,
            'images'       => $this->images,
            'references'   => $this->references,
            'tags'         => $this->tags,
            'creationDate' => $this->creationDate,
        ];
    }
}
