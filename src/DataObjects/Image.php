<?php

declare(strict_types=1);

namespace FlyingAnvil\RelicDbApi\DataObjects;

use FlyingAnvil\Libfa\DataObject\DataObject;
use FlyingAnvil\Libfa\DataObject\Id\SmallId;
use JetBrains\PhpStorm\Immutable;

#[Immutable]
final class Image implements DataObject
{
    private function __construct(
        private SmallId $id,
        private string $source,
        private string $title,
    ) {}

    public static function create(SmallId $id, string $source, string $title): self
    {
        return new self($id, $source, $title);
    }

    public function getId(): SmallId
    {
        return $this->id;
    }

    public function getSource(): string
    {
        return $this->source;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function jsonSerialize(): array
    {
        return [
            'id'     => $this->id,
            'source' => $this->source,
            'title'  => $this->title,
        ];
    }
}
