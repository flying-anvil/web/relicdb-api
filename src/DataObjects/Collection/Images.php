<?php

declare(strict_types=1);

namespace FlyingAnvil\RelicDbApi\DataObjects\Collection;

use FlyingAnvil\Libfa\DataObject\DataObject;
use FlyingAnvil\RelicDbApi\DataObjects\Image;
use Generator;
use IteratorAggregate;
use JetBrains\PhpStorm\Immutable;

#[Immutable]
final class Images implements DataObject, IteratorAggregate, \Countable
{
    /** @var Image[] */
    private array $images;

    private function __construct(array $images)
    {
        $this->images = $images;
    }

    public static function create(Image ...$images): self
    {
        return new self($images);
    }

    public function jsonSerialize(): array
    {
        return $this->images;
    }

    /**
     * @return Generator | Image[]
     */
    public function getIterator(): Generator
    {
        yield from $this->images;
    }

    public function count(): int
    {
        return count($this->images);
    }
}
