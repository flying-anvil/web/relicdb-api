<?php

declare(strict_types=1);

namespace FlyingAnvil\RelicDbApi\DataObjects\Collection;

use FlyingAnvil\Libfa\DataObject\DataObject;
use FlyingAnvil\RelicDbApi\DataObjects\Reference;
use Generator;
use IteratorAggregate;

final class References implements DataObject, IteratorAggregate, \Countable
{
    /** @var Reference[] */
    private array $references;

    private function __construct(array $references)
    {
        $this->references = $references;
    }

    public static function create(Reference ...$references): self
    {
        return new self($references);
    }

    public function jsonSerialize(): array
    {
        return $this->references;
    }

    /**
     * @return Generator | Reference[]
     */
    public function getIterator(): Generator
    {
        yield from $this->references;
    }

    public function count(): int
    {
        return count($this->references);
    }
}
