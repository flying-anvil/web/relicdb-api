<?php

declare(strict_types=1);

namespace FlyingAnvil\RelicDbApi\DataObjects\Collection;

use FlyingAnvil\Libfa\DataObject\DataObject;
use FlyingAnvil\RelicDbApi\DataObjects\Tag;
use Generator;
use IteratorAggregate;
use JetBrains\PhpStorm\Immutable;

#[Immutable]
final class Tags implements DataObject, IteratorAggregate, \Countable
{
    /** @var Tag[] */
    private array $tags;

    private function __construct(array $tags)
    {
        $this->tags = $tags;
    }

    public static function create(Tag ...$tags): self
    {
        return new self($tags);
    }

    public function jsonSerialize(): array
    {
        return $this->tags;
    }

    public function getIterator(): Generator
    {
        yield from $this->tags;
    }

    public function count(): int
    {
        return count($this->tags);
    }
}
