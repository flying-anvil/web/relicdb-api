<?php

declare(strict_types=1);

namespace FlyingAnvil\RelicDbApi\DataObjects\Collection;

use FlyingAnvil\Libfa\DataObject\DataObject;
use FlyingAnvil\RelicDbApi\DataObjects\Gemstone;
use Generator;
use IteratorAggregate;

final class Gemstones implements DataObject, IteratorAggregate, \Countable
{
    /** @var Gemstone[] */
    private array $gemstones;

    private function __construct(array $gemstones)
    {
        $this->gemstones = $gemstones;
    }

    public static function create(Gemstone ...$gemstones): self
    {
        return new self($gemstones);
    }

    public function jsonSerialize(): array
    {
        return $this->gemstones;
    }

    public function getIterator(): Generator
    {
        yield from $this->gemstones;
    }

    public function count(): int
    {
        return count($this->gemstones);
    }
}
